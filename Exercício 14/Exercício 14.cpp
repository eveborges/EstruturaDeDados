#include <iostream>

using namespace std;

const int tamanho = 10;

int* vetor[tamanho];

void inicializarVetor(int tamanho){
	
	for (int i=0; i < tamanho; i++){
		vetor[i] = new int;
		*(vetor[i]) = 0;
	}
}

void lerVetor(int tamanho){  
    
	*vetor[0] = 6;
	*vetor[1] = 3;
	*vetor[2] = 18;
	*vetor[3] = 23;
	*vetor[4] = -72;
	*vetor[5] = -6;
	*vetor[6] = -3;
	*vetor[7] = 1;
	*vetor[8] = -999;
	*vetor[9] = 999;
	
}

void imprimirVetor(int tamanho){
	
	cout << endl << endl << "|Valores ordenados Bubble Sort: ";
			
		
	cout << "\n|------------------------------|\n";
	for (int i=0; i < tamanho; i++){
        cout << "|Posicao "<< (i+1) <<": "<< *vetor[i] << endl;
    }
    cout << "\n|------------------------------|\n";
}

void qs(int left,int right);

void quicksort(int left,int right){
    qs(left,right-1);
}

void qs(int left,int right){

    int i,j;
    i = left;
    j = right;
    int meio = *vetor[(left+right)/2];

    while (j > i){
        while (*vetor[i]<meio && i<right) i++;
        while (meio<*vetor[j] && j>left) j--;

        if (i<=j){
            int temp = *vetor[i];
            *vetor[i] = *vetor[j];
            *vetor[j] = temp;
            i++;
            j--;
        }
    }

    if (left < j) qs(left,j);
    if (i < right) qs(i,right);

}



int main()
{
	
	inicializarVetor(tamanho);
	lerVetor(tamanho);
	quicksort(0,tamanho);
    imprimirVetor(tamanho);

	
	return 0;

}

