#include <iostream>

using namespace std;

const int altura = 3;
const int largura = 3;

void inicializarMatriz(int matriz[altura][largura], int altura, int largura){
	cout << "Inicializando a matriz " << endl;
	
	for (int i=0; i < altura; i++){
		for(int j = 0; j < largura; j++){
			matriz[i][j] = 0;
		}
	}
}

void lerMatriz(int matriz[altura][largura], int altura, int largura){
    cout << "Leia os valores da matriz" << endl;
    
	for (int i=0; i < altura; i++){
		for (int j=0; j < largura; j++){
			cout << "Posicao ["<< (i) << "][" << (j) <<"]: ";
			cin >> matriz[i][j];
		}
	}
}

void imprimirMenorValorMatriz(int matriz[altura][largura],int altura, int largura){
    int menorValor = matriz[0][0];
    
	for (int i=0; i < altura; i++){
        for (int j=0; j < largura; j++){			
			if(matriz[i][j] < menorValor){
				menorValor = matriz[i][j];
			}
		}
    }
    
    cout << endl << "Menor Valor: " << menorValor << endl;
}

void finalizarPrograma(){
	cout << "Fim do Programa!!"<< endl;
}

int main()
{
	int matriz[altura][largura];
	
	inicializarMatriz(matriz,altura,largura);
	
	lerMatriz(matriz,altura,largura);	
	
	imprimirMenorValorMatriz(matriz,altura,largura);
	
	finalizarPrograma();
	
	return 0;	
}


