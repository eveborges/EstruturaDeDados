#include <iostream>

using namespace std;

const int tamanho = 10;

int* vetor[tamanho];

void inicializarVetor(int* vetor[], int tamanho){
	cout << "Inicializando o vetor " << endl;
	
	for (int i=0; i < tamanho; i++){
		vetor[i]=new int;
		*(vetor[i])=0;
	}
}

void lerVetor(int* vetor[], int tamanho){
    cout << "Leia os valores do vetor" << endl;
    
	for (int i=0; i < tamanho; i++){
		cout << "Posicao "<<(i+1)<<": ";
		cin >> *(vetor[i]);
	}
}

void imprimirVetor(int* vetor[],int tamanho){
    for (int i=0; i < tamanho; i++){
        cout << "Posicao "<< (i+1) <<": "<< *vetor[i]<<endl;
    }
}

void finalizarPrograma(){
	cout << "Fim do Programa!!"<< endl;
}

int main()
{

	inicializarVetor(vetor,tamanho);
	
	lerVetor(vetor,tamanho);
	
	imprimirVetor(vetor,tamanho);
	
	finalizarPrograma();
	
	return 0;

}

