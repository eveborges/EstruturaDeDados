#include <iostream>

using namespace std;

const int tamanho = 20;

int* vetor[tamanho];

void inicializarVetor(int tamanho){
	
	for (int i=0; i < tamanho; i++){
		vetor[i] = new int;
		*(vetor[i]) = 0;
	}
}

void lerVetor(int tamanho){
    cout << "\nLeia os valores do vetor" << endl;
    
	for (int i=0; i < tamanho; i++){
		cout << "Posicao " << (i+1) << ": ";
		cin >> *vetor[i];
	}
}

void imprimirVetor(int tamanho){
	
    cout << endl << endl << "|Valores ordenados selectionSort: ";
	cout << "\n|--------------------------------|\n";
	for (int i=0; i < tamanho; i++){
        cout << "|Posicao "<< (i+1) <<": "<< *vetor[i] << endl;
    }
    cout << "\n|--------------------------------|\n";
}

void selectionSort(int tamanho){
    for (int i=0; i < tamanho; i++){
        int menor = i;
        
		for (int j=i+1; j < tamanho; j++){
            if (*vetor[j] < *vetor[menor]){
                menor = j;
            }
        }
        
		if (i != menor){
            int temp = *vetor[i];
            *vetor[i] = *vetor[menor];
            *vetor[menor] = temp;
        }
    }
}

void finalizarPrograma(){
	cout << endl << "Fim do Programa!!"<< endl;
}

int main()
{

	inicializarVetor(tamanho);
	
	lerVetor(tamanho);
	
	selectionSort(tamanho);
	
	imprimirVetor(tamanho);
	
	finalizarPrograma();
	
	return 0;

}

