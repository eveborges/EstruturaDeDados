#include <iostream>

using namespace std;

const int tamanho = 10;

void inicializarVetor(int* vetor[], int tamanho){
	
	for (int i=0; i < tamanho; i++){
		vetor[i]=new int;
		*(vetor[i])=0;
	}
}

void lerVetor(int* vetor[], int tamanho){
    
	for (int i=0; i < tamanho; i++){
		cout << "Posicao "<<(i+1)<<": ";
		cin >> *(vetor[i]);
	}
}

void imprimirMenorMaiorValorMedia(int* vetor[], int tamanho){
    int menorValor = *(vetor[0]);
    int maiorValor = *(vetor[0]);
    float media = 0;
    
    for (int i=0; i<tamanho; i++){			
		if(*(vetor[i]) < menorValor){
			menorValor = *(vetor[i]);
		}
		
		if(*(vetor[i]) > maiorValor){
			maiorValor = *(vetor[i]);
		}
		
		media += *(vetor[i]);
	}
	
	media = media / tamanho;
    
    cout << endl << "Menor Valor: " << menorValor << endl;
    cout << "Maior Valor: " << maiorValor << endl;
    cout << "Media dos Valores: " << media << endl;
}

int main()
{
	int* vetor[tamanho];

	inicializarVetor(vetor,tamanho);
	
	*vetor[0] = 1;
	*vetor[1] = 2;
	*vetor[2] = 3;
	*vetor[3] = 4;
	*vetor[4] = 5;
	*vetor[5] = 0;
	*vetor[6] = 7;
	*vetor[7] = 8;
	*vetor[8] = 9;
	*vetor[9] = 10;
	
	//lerVetor(vetor,tamanho);
	
	imprimirMenorMaiorValorMedia(vetor,tamanho);
	
	return 0;

}

