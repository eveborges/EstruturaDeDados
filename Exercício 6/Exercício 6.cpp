#include <iostream>

using namespace std;

const int tamanho = 15;

int* vetor[tamanho];

void inicializarVetor(int* vetor[], int tamanho){
	
	for (int i=0; i < tamanho; i++){
		vetor[i]=new int;
		*(vetor[i]) = i;
	}
}

void inicializarVetorAux(int* vetorAux[], int tamanho){
	
	for (int i=0; i < tamanho; i++){
		vetorAux[i]=new int;
		*(vetorAux[i]) = i;
	}
}

void copiarVetor(int* vetor[], int* vetorAux[], int tamanho){
	
	for (int i=0; i < tamanho; i++){
		*(vetor[i]) = *(vetorAux[i]);
	}
}

void imprimirVetor(int* vetor[],int tamanho){
    for (int i=0; i < tamanho; i++){
        cout << "Posicao " << (i+1) << ": " << *vetor[i] << endl;
    }
}

void imprimirVetorInvertido(int* vetor[], int tamanho){
    
	int* vetorAux[tamanho];
    inicializarVetorAux(vetorAux,tamanho);
    
    int j = 0;
    for (int i=tamanho-1; i >= 0; i--){			
		*vetorAux[j] = *vetor[i];
		j += 1;	
	}    
	
	copiarVetor(vetor,vetorAux,tamanho);
	imprimirVetor(vetor,tamanho);

}

void finalizarPrograma(){
	cout << endl << "Fim do Programa!!"<< endl;
}

int main()
{

	inicializarVetor(vetor,tamanho);
	
	imprimirVetorInvertido(vetor,tamanho);
	
	finalizarPrograma();
	
	return 0;

}

