#include <iostream>
#include <stdio.h>      /* printf, scanf, NULL */
#include <stdlib.h>

using namespace std;
int* vetor;

int criarInicializarVetor(){
		
	int tam;
	
	cout << "Digite o tamanho do vetor: ";
	cin >> tam;
		
	vetor = (int*) malloc (tam * sizeof(int));
		
	for (int i=0; i < tam; i++){
		vetor[i] = 0;
	}
	
	return tam;
}


void lerVetor(int tamanho){
    cout << "\nLeia os valores do vetor" << endl;
    
	for (int i=0; i < tamanho; i++){
		cout << "Posicao " << (i+1) << ": ";
		cin >> vetor[i];
	}
}

void imprimirVetor(int tamanho){
	
    cout << endl << endl << "|Valores digitados: ";
	cout << "\n|------------------|\n";
	for (int i=0; i < tamanho; i++){
        cout << "|Posicao "<< (i+1) <<": "<< vetor[i] << endl;
    }
    cout << "|------------------|\n";
}

void finalizarPrograma(){
	cout << "Fim do Programa!!"<< endl;
}

int main()
{
	int tamanho;
		
	tamanho = criarInicializarVetor();
	
	lerVetor(tamanho);
	
	imprimirVetor(tamanho);
	
	finalizarPrograma();
	
	return 0;

}

