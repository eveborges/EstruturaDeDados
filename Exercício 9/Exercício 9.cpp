#include <iostream>

using namespace std;

const int tamanho = 10;

int* vetor[tamanho];

void inicializarVetor(int tamanho){
	
	for (int i=0; i < tamanho; i++){
		vetor[i] = new int;
		*(vetor[i]) = 0;
	}
}

void lerVetor(int tamanho){
    cout << "\nLeia os valores do vetor" << endl;
    
	for (int i=0; i < tamanho; i++){
		cout << "Posicao " << (i+1) << ": ";
		cin >> *vetor[i];
	}
}

void imprimirVetor(int tamanho){
	
    cout << endl << endl << "|Valores ordenados bubbleSort: ";
	cout << "\n|------------------------------|\n";
	for (int i=0; i < tamanho; i++){
        cout << "|Posicao "<< (i+1) <<": "<< *vetor[i] << endl;
    }
    cout << "\n|------------------------------|\n";
}

void bubbleSort(int tamanho){
	int aux;
	
	for(int j=tamanho-1; j>=1; j--){
		
		for(int i=0; i<j; i++){
		
			if(*vetor[i] > *vetor[i+1]) {
				
				aux = *vetor[i];
				*vetor[i] = *vetor[i+1];
				*vetor[i+1]= aux;
			}
		}
	}
}

void finalizarPrograma(){
	cout << endl << "Fim do Programa!!"<< endl;
}

int main()
{

	inicializarVetor(tamanho);
	
	lerVetor(tamanho);
	
	bubbleSort(tamanho);
	
	imprimirVetor(tamanho);
	
	finalizarPrograma();
	
	return 0;

}

