#include <iostream>

using namespace std;

const int altura = 3;
const int largura = 3;

void inicializarMatriz(int matriz[altura][largura], int altura, int largura){
	
	for (int i=0; i < altura; i++){
		for(int j = 0; j < largura; j++){
			matriz[i][j] = 0;
		}
	}
}

void lerMatriz(int matriz[altura][largura], int altura, int largura){
    
    cout << "\nDigite os dados da Matriz:\n";
	for (int i=0; i < altura; i++){
		for (int j=0; j < largura; j++){
			cout << "Posicao ["<< (i) << "][" << (j) <<"]: ";
			cin >> matriz[i][j];
		}
	}
}

void somarMatrizes(int matriz1[altura][largura], int matriz2[altura][largura], int matrizSoma[altura][largura], int altura, int largura){
    	   
	for (int i=0; i < altura; i++){
        for (int j=0; j < largura; j++){			
			matrizSoma[i][j] = matriz1[i][j] + matriz2[i][j];
		}
    }
	
	cout << endl << "Matriz Soma: " << endl;
	for (int i=0; i < altura; i++){
        for (int j=0; j < largura; j++){
        	cout << matrizSoma[i][j] << " ";
        }
        cout << endl;
	}
    
}

void multiplicarMatrizes(int matriz1[altura][largura], int matriz2[altura][largura], int matrizMultp[altura][largura], int altura, int largura){
    int soma = 0;
    
	for (int i=0; i < altura; i++){
        
		for (int j=0; j < largura; j++){
			soma = 0;
			
			for (int k=0; k < largura; k++){
				soma = soma + matriz1[i][k] * matriz2[k][j];
				matrizMultp[i][j] = soma;				
			}
		}
    }
    
    cout << endl << "Matriz Multiplicacao: " << endl;
    for (int i=0; i < altura; i++){
        for (int j=0; j < largura; j++){
        	cout << matrizMultp[i][j] << " ";
        }
        cout << endl;
	}

}

int main()
{
	int matriz1[altura][largura];
	int matriz2[altura][largura];
	int matrizSoma[altura][largura];
	int matrizMultiplicacao[altura][largura];

	inicializarMatriz(matriz1,altura,largura);
	inicializarMatriz(matriz2,altura,largura);
	
	lerMatriz(matriz1,altura,largura);	
	lerMatriz(matriz2,altura,largura);	
	
	somarMatrizes(matriz1,matriz2,matrizSoma,altura,largura);
	multiplicarMatrizes(matriz1,matriz2,matrizMultiplicacao,altura,largura);
		
	return 0;	
}


