#include <iostream>
#include <string.h>

using namespace std;

const int tamanho = 2;
int op = 0;

struct Registro{
	int codigo;
	char nome[60];
	char endereco[60];
};

struct Registro reg[tamanho];

void inicializarVetor(Registro *reg, int tamanho){
	
	for (int i=0; i < tamanho; i++){
		reg[i].codigo = 0;
		strcpy(reg[i].nome, " ");
		strcpy(reg[i].endereco, " ");
	}
}

void lerVetor(Registro* reg, int tamanho){
    cout << "Digite as informacoes pedidas.\n" << endl;
    
	for (int i=0; i < tamanho; i++){
		cout << "Funcionario "<<(i+1)<< endl << "Codigo: ";
		cin >> reg[i].codigo;
		cout << "Nome: ";
		cin >> reg[i].nome;
		cout << "Endereco: ";
		cin >> reg[i].endereco;
	}
}

void menu(){
	cout << "\n|----------------------------------|\n";
	cout << "  Digite o Codigo do Funcionario\n  ou\n";
	cout << "  Digite 9 - Sair\n";
	cout << "|----------------------------------|\n-> ";
	cin >> op;
}

void pesquisarFuncionarioPorCodigo(){
	
	bool regEncontrado = false;		
	
	for (int i=0; i < tamanho; i++){
		if (reg[i].codigo == op) {
			cout << "\nFuncionario "<< (i+1) ;
			cout << "\nCodigo: " << reg[i].codigo;
			cout << "\nNome: " << reg[i].nome;
			cout << "\nEndereco: " << reg[i].endereco;
			cout << endl;
			
			regEncontrado = true;
		}
	}
	
	
	if ( ! regEncontrado ) {
		cout << "\nFuncionario nao encontrado\n";		
	}
}

void finalizarPrograma(){
	cout << endl << "Fim do Programa!!"<< endl;
}

int main()
{

	inicializarVetor(reg,tamanho);
	lerVetor(reg,tamanho);
	
	do {
		op = 0;
		
		menu();
		pesquisarFuncionarioPorCodigo();
		
	} while ( op != 9 );
	
	finalizarPrograma();
	
	return 0;

}

