#include <iostream>

using namespace std;

const int tamanho = 5;

struct pontos {
    int x;
    int y;
};

pontos vetPontos[tamanho];

void lerPontos(pontos vetPontos[], int tamanho){
    for (int i=0; i < tamanho; i++){
        cout << "Digite o ponto x"<<(i+1)<<": ";       
		cin >> vetPontos[i].x;
        
		cout << "Digite o ponto y"<<(i+1)<<": ";       
        cin >> vetPontos[i].y;  
		
		cout << "\n";
    }
}

void maiorValorX(pontos vetPontos[], int tamanho){
	int maiorX = vetPontos[0].x;
		
	for( int i = 0 ; i < tamanho ; i++ ){
		if( vetPontos[i].x > maiorX ){
			maiorX = vetPontos[i].x;
		}
		
	}
	
	cout << "\nMaior valor de X: " << maiorX;
}

void menorValorY(pontos vetPontos[], int tamanho){
	int menorY = vetPontos[0].y;
		
	for( int i = 0 ; i < tamanho ; i++ ){
		if( vetPontos[i].y < menorY ){
			menorY = vetPontos[i].y;
		}
		
	}
	
	cout << "\nMenor valor de Y: " << menorY;
}

void buscarPosicaoValor(pontos vetPontos[], int tamanho){
	int valor;
			
	cout << "\n\n-------------------\n" << "Digite o valor: ";
	cin >> valor;
	cout << endl;
	
	for( int i = 0 ; i < tamanho ; i++ ){
		
		if( vetPontos[i].x == valor ){
			cout << "Valor " << valor << " encontrado na posicao x" << i+1 << endl;
		}
		
		if( vetPontos[i].y == valor ){
			cout << "Valor " << valor << " encontrado na posicao y" << i+1 << endl;
		}
		
	}
}

int main()
{
 
	cout << "Pontos \n" << endl;
    
	lerPontos(vetPontos, tamanho);
    
	maiorValorX(vetPontos, tamanho);
    
	menorValorY(vetPontos, tamanho);
    
	buscarPosicaoValor(vetPontos, tamanho);
    
	return 0;
}

